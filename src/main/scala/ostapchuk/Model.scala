package ostapchuk

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.{col, expr, max, _}

class Model(args: Array[String], spark: SparkSession) {
    val staticExpediaPath: String = args(0)
    val checkPoint = args(1)
    val hdfsPath = args(2)
    val input = new Input(spark, staticExpediaPath)

    import spark.implicits._

    val staticExpedia = spark.sparkContext.broadcast(input.getStaticExpedia())

    def start(): Unit = {
        if (args.length != 3) {
            println("Usage: <path in hdfs to static expedia> <checkpoint location> <path in hdfs to write results to>")
            return
        } else write(joinHotelsExpedia(), hdfsPath)
    }

    def joinHotelsExpedia(): DataFrame = {
        val hotelsWeather = joinHotelsWeather()

        val expedia16 = staticExpedia.value

        val expedia17 = input.getStreamExpedia()
            .withWatermark("time_stamp", "15 minutes")

        val firstJoin = hotelsWeather.join(expedia16,
            hotelsWeather("wthr_date") <=> expedia16("srch_ci16")
                && hotelsWeather("id") <=> expedia16("hotel_id16"),
            "leftOuter")
            .withWatermark("time_stampH", "15 minutes")

        val secondJoin = firstJoin.join(expedia17,
            expr(
                """
                    wthr_date = srch_ci17 AND
                    id = hotel_id17 AND
                    time_stampH BETWEEN
                    time_stamp - INTERVAL 10 minutes AND
                    time_stamp + INTERVAL 10 minutes
                """), "leftOuter")
            .withColumn("hotel_id", coalesce($"hotel_id16", $"hotel_id17"))
            .filter($"hotel_id".isNotNull)
            .withColumn("srch_co", coalesce($"srch_co16", $"srch_co17"))
            .withColumn("srch_ci", coalesce($"srch_ci16", $"srch_ci17"))
            .withColumn("srch_children_cnt", coalesce($"srch_children_cnt16", $"srch_children_cnt17"))
            .withColumn("children_present", coalesce($"children_present16", $"children_present17"))
            .withColumn("stay_duration", coalesce($"stay_duration16", $"stay_duration17"))
            .filter($"hotel_id".isNotNull)
            .select(col("hotel_id"), col("lng"), col("lat"),
                col("avg_tmpr_c"), col("srch_ci"), col("srch_co"),
                col("srch_children_cnt"), col("children_present"),
                col("stay_duration"))

        return secondJoin
    }

    def joinHotelsWeather(): DataFrame = {
        val hotels = input.getStreamHotels()
            .withColumn("time_stampH", lit(current_timestamp()))
            .withWatermark("time_stampH", "15 minutes")

        val weather = input.getStreamWeather()
            .withColumn("time_stamp", lit(current_timestamp()))
            .withWatermark("time_stamp", "15 minutes")

        val hotelsWeather = hotels.join(weather,
            expr(
                """
                   latH=lat AND
                   lngH=lng AND
                    time_stampH BETWEEN
                    time_stamp - INTERVAL 10 minutes AND
                    time_stamp + INTERVAL 10 minutes
                 """), "leftOuter")

        val filterCond = hotelsWeather.columns.map(x => col(x).isNotNull).reduce(_ && _)

        val hotelsWeatherFiltered = hotelsWeather
            .filter(filterCond)

        return hotelsWeatherFiltered.drop("time_stamp")
    }

    def write(toWrite: DataFrame, path: String): Unit = {
        val testConsole = toWrite
            .writeStream
            .format("csv")
            .outputMode("append")
            .option("path", hdfsPath)
            .option("checkpointLocation", checkPoint)
            .option("sep", ",")
            .option("header", "true")
            .start()

        testConsole.awaitTermination(3600000)
    }

    def writeKafka(toWrite: DataFrame): Unit = {
        val kafka = toWrite
            .selectExpr("CAST(id AS STRING) AS key", "to_json(struct(*)) AS value")
            .writeStream
            .format("kafka")
            .outputMode("append")
            .option("kafka.bootstrap.servers", ":6667")
            .option("topic", "results")
            .option("kafka.linger.ms", 10000)
            .option("kafka.request.timeout.ms", 7000)
            .option("checkpointLocation", checkPoint)
            .start()

        kafka.awaitTermination(900000)
    }

    def createCustomerPreferences(dataFrame: DataFrame): Unit = {
        val customerPreferences = dataFrame
            .groupBy("hotel_id17", "stay_type17")
            .count()
            .groupBy("hotel_id17")
            .agg(max(struct("count", "stay_type17")).as("max17"))
            .select($"hotel_id17",
                $"max17.count".as("max17"),
                $"max17.stay_type17".as("customer_preference17"))
    }
}