package ostapchuk

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._

class Input(spark: SparkSession, staticExpediaPath: String) {

    import spark.implicits._

    val kafkaIP = "35.223.226.108"
    val kafkaOffset = 1000

    def readHotelsKafka(): DataFrame = {
        val hotels = spark
            .readStream
            .format("kafka")
            .option("kafka.bootstrap.servers", kafkaIP + ":6667")
            .option("subscribe", "hotels")
            .option("maxOffsetsPerTrigger", kafkaOffset)
            .option("startingOffsets", "earliest")
            .load()

        val keyValue = hotels.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
            .as[(String, String)].toDF()

        val schema = Utils.kafkaHotelSchema

        val toWrite = keyValue.select(from_json(col("value").cast("string"),
            schema).as("data"))
            .select("data.*")
            .select("Id", "Latitude", "Longitude") //add Name

        return toWrite
    }

    def getStreamHotels(): DataFrame = {
        val hotels = readHotelsKafka()
            .withColumnRenamed("Id", "id")
            .withColumnRenamed("Name", "name")
            .withColumnRenamed("Latitude", "latH")
            .withColumnRenamed("Longitude", "lngH")
            .withColumn("latH", round($"latH", 2))
            .withColumn("lngH", round($"lngH", 2))

        return hotels
    }

    def readWeatherKafka(): DataFrame = {
        val hotels = spark
            .readStream
            .format("kafka")
            .option("kafka.bootstrap.servers", kafkaIP + ":6667")
            .option("subscribe", "weather")
            .option("maxOffsetsPerTrigger", kafkaOffset * 5)
            .option("startingOffsets", "earliest")
            .load()

        val keyValue = hotels.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
            .as[(String, String)].toDF()

        val schema = Utils.kafkaWeatherSchema

        val toWrite = keyValue.select(from_json(col("value").cast("string"),
            schema).as("data"))
            .select("data.*")
            .drop("avg_tmpr_f", "year", "month", "day")

        return toWrite
    }

    def getStreamWeather(): DataFrame = {
        val weather = readWeatherKafka()
            .select("lng", "lat", "avg_tmpr_c", "wthr_date")
            .filter(col("avg_tmpr_c") > 0)
            .withColumn("lat", round($"lat", 2))
            .withColumn("lng", round($"lng", 2))

        return weather
    }

    def getStaticExpedia(): DataFrame = {
        val expedia16 = spark.read
            .format("csv")
            .option("header", "true")
            .load(staticExpediaPath)
            .select(
                col("srch_ci"), col("srch_co"),
                col("hotel_id"), col("srch_children_cnt"))
            .withColumn("children_present", when(col("srch_children_cnt") <=> 0, "no")
                .otherwise("yes"))
            .withColumn("stay_duration16", datediff(col("srch_co"), col("srch_ci")))
            .withColumn("stay_type16",
                when(col("stay_duration16") >= 1 &&
                    col("stay_duration16") <= 3,
                    "Short stay")
                    .when(col("stay_duration16") >= 4 &&
                        col("stay_duration16") <= 7,
                        "Standart stay")
                    .when(col("stay_duration16") >= 8 &&
                        col("stay_duration16") <= 14,
                        "Standart extended stay")
                    .when(col("stay_duration16") >= 15 &&
                        col("stay_duration16") <= 30,
                        "Long stay")
                    .otherwise("Erroneous data"))
            .withColumnRenamed("srch_co", "srch_co16")
            .withColumnRenamed("srch_children_cnt", "srch_children_cnt16")
            .withColumnRenamed("hotel_id", "hotel_id16")
            .withColumnRenamed("srch_ci", "srch_ci16")
            .withColumnRenamed("children_present", "children_present16")

        val filterCond16 = expedia16.columns.map(x => col(x).isNotNull).reduce(_ && _)

        val expedia16ToJoin = expedia16
            .filter(filterCond16)
            .where($"srch_ci".contains("2016"))

        val customerPreferences16 = expedia16ToJoin
            .groupBy("hotel_id16", "stay_type16")
            .count()
            .withColumn("max_stay_type16",
                max("count") over Window.partitionBy("hotel_id16"))
            .filter(col("count") === col("max_stay_type16"))
            .withColumnRenamed("stay_type16", "customer_preference16")
            .drop("count")
            .withColumnRenamed("hotel_id16", "hotel_id")

        val filteredExpedia16 = expedia16ToJoin.join(customerPreferences16,
            customerPreferences16("hotel_id") <=> expedia16ToJoin("hotel_id16"))

        return filteredExpedia16
    }

    def readExpediaKafka(): DataFrame = {
        val hotels = spark
            .readStream
            .format("kafka")
            .option("kafka.bootstrap.servers", kafkaIP + ":6667")
            .option("subscribe", "expedia")
            .option("maxOffsetsPerTrigger", kafkaOffset * 2)
            .option("startingOffsets", "earliest")
            .load()

        val keyValue = hotels.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
            .as[(String, String)].toDF()

        val schema = Utils.kafkaExpediaSchema

        val toWrite = keyValue.select(from_json(col("value").cast("string"),
            schema).as("data"))
            .select("data.*")
            .select(
                col("srch_ci"), col("srch_co"),
                col("hotel_id"), col("srch_children_cnt"))
            .withColumn("time_stamp", lit(current_timestamp()))

        return toWrite
    }

    def getStreamExpedia(): DataFrame = {
        val expedia17 = readExpediaKafka()
            .withColumn("children_present", when(col("srch_children_cnt") <=> 0, "no")
                .otherwise("yes"))
            .withColumn("stay_duration17", datediff(col("srch_co"), col("srch_ci")))
            .withColumn("stay_type17",
                when(col("stay_duration17") >= 1 &&
                    col("stay_duration17") <= 3,
                    "Short stay")
                    .when(col("stay_duration17") >= 4 &&
                        col("stay_duration17") <= 7,
                        "Standart stay")
                    .when(col("stay_duration17") >= 8 &&
                        col("stay_duration17") <= 14,
                        "Standart extended stay")
                    .when(col("stay_duration17") >= 15 &&
                        col("stay_duration17") <= 30,
                        "Long stay")
                    .otherwise("Erroneous data"))
            .withColumnRenamed("srch_co", "srch_co17")
            .withColumnRenamed("srch_children_cnt", "srch_children_cnt17")
            .withColumnRenamed("hotel_id", "hotel_id17")
            .withColumnRenamed("srch_ci", "srch_ci17")
            .withColumnRenamed("children_present", "children_present17")

        val filterCond17 = expedia17.columns.map(x => col(x).isNotNull).reduce(_ && _)

        val expedia17ToJoin = expedia17
            .filter(filterCond17)
            .where($"srch_ci".contains("2017"))

        return expedia17ToJoin
    }
}
