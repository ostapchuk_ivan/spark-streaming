package ostapchuk

import org.apache.spark.sql.types.{DataType, StructType}

object Utils {
    val kafkaHotelJson =
        """{"type":"struct","fields":[{"name":"Id","type":"string","nullable":true,"metadata":{}},{"name":"Name","type":"string","nullable":true,"metadata":{}},
          |{"name":"Country","type":"string","nullable":true,"metadata":{}},{"name":"City","type":"string","nullable":true,"metadata":{}},
          |{"name":"Address","type":"string","nullable":true,"metadata":{}},{"name":"Latitude","type":"string","nullable":true,"metadata":{}},{"name":"Longitude","type":"string","nullable":true,"metadata":{}}]}""".stripMargin
    val kafkaHotelSchema = DataType.fromJson(kafkaHotelJson).asInstanceOf[StructType]

    val kafkaExpediaJson = """{"type":"struct","fields":[{"name":"id","type":"long","nullable":true,"metadata":{}},{"name":"date_time","type":"string","nullable":true,"metadata":{}},{"name":"site_name","type":"integer","nullable":true,"metadata":{}},{"name":"posa_continent","type":"integer","nullable":true,"metadata":{}},{"name":"user_location_country","type":"integer","nullable":true,"metadata":{}},{"name":"user_location_region","type":"integer","nullable":true,"metadata":{}},{"name":"user_location_city","type":"integer","nullable":true,"metadata":{}},{"name":"orig_destination_distance","type":"double","nullable":true,"metadata":{}},{"name":"user_id","type":"integer","nullable":true,"metadata":{}},{"name":"is_mobile","type":"integer","nullable":true,"metadata":{}},{"name":"is_package","type":"integer","nullable":true,"metadata":{}},{"name":"channel","type":"integer","nullable":true,"metadata":{}},{"name":"srch_ci","type":"string","nullable":true,"metadata":{}},{"name":"srch_co","type":"string","nullable":true,"metadata":{}},{"name":"srch_adults_cnt","type":"integer","nullable":true,"metadata":{}},{"name":"srch_children_cnt","type":"integer","nullable":true,"metadata":{}},{"name":"srch_rm_cnt","type":"integer","nullable":true,"metadata":{}},{"name":"srch_destination_id","type":"integer","nullable":true,"metadata":{}},{"name":"srch_destination_type_id","type":"integer","nullable":true,"metadata":{}},{"name":"hotel_id","type":"long","nullable":true,"metadata":{}}]}"""
    val kafkaExpediaSchema = DataType.fromJson(kafkaExpediaJson).asInstanceOf[StructType]

    val kafkaWeatherJson =
        """{"type":"struct","fields":[{"name":"lng","type":"double","nullable":true,"metadata":{}},{"name":"lat","type":"double","nullable":true,"metadata":{}},
          |{"name":"avg_tmpr_f","type":"double","nullable":true,"metadata":{}},{"name":"avg_tmpr_c","type":"double","nullable":true,"metadata":{}},{"name":"wthr_date","type":"string","nullable":true,"metadata":{}}]}""".stripMargin
    val kafkaWeatherSchema = DataType.fromJson(kafkaWeatherJson).asInstanceOf[StructType]
}
