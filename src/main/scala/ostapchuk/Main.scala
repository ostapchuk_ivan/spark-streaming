package ostapchuk

import org.apache.spark.sql.{SparkSession}

/**
  * erform stateful streaming with Spark Structured Streaming.
  *
  * Read Expedia data for 2016 year from HDFS as initial state DataFrame.
  *
  * Read data for 2017 year as streaming data.
  *
  * Enrich both DataFrames with weather:
  * add average day temperature at checkin
  * (join with hotels+weather data from Kafka topic).
  *
  * Filter incoming data by having average temperature more than 0 Celsius degrees.
  *
  * Calculate customer's duration of stay as days
  * between requested check-in and check-out date.
  *
  * Create customer preferences of stay time based on next logic.
  *
  * Map each hotel with multi-dimensional state consisting of
  * record counts for each type of stay:
  * "Erroneous data": null, more than month(30 days), less than or equal to 0
  * "Short stay": 1-3 days
  * "Standart stay": 4-7 days
  * "Standart extended stay": 1-2 weeks
  * "Long stay": 2-4 weeks (less than month)
  * And resulting type for a hotel (with max count)
  *
  * Apply additional variance with filter
  * on children presence in booking (x2 possible states).
  *
  * Store final data in HDFS.
  */

object Main {
    def main(args: Array[String]): Unit = {
        val spark = SparkSession
            .builder
            .appName("StructuredNetworkWordCount")
            .master("local[6]")
            .getOrCreate()

        val model = new Model(args, spark)
        model.start()
    }
}